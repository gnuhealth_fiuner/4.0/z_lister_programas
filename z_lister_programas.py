# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta

from trytond.model import ModelSQL, ModelView, fields

__all__ = ['SaludSexual', 'LecheMaternaMadres', 'LecheMaternaMenores']


class SaludSexual(ModelSQL, ModelView):
    'Programa de Salud Sexual y Reproductiva'
    __name__ = 'programas.salud_sexual'

    fecha = fields.Date('Fecha de Entrega', required=True)
    usuario = fields.Many2One('gnuhealth.patient', 'Apellido y Nombre',
        required=True)
    dni = fields.Function(fields.Char('DNI'), 'get_dni')
    edad = fields.Function(fields.Char('Edad',
        help='Edad a la fecha de la entrega'), 'get_patient_age')
    contacto = fields.Char('Medio de Contacto', size=40)
    producto1 = fields.Many2One('product.product', 'Producto Mes 1',
        help='Producto')
    cantidad = fields.Integer('Cantidad')
    producto2 = fields.Many2One('product.product', 'Producto Mes 2',
        help='Producto')
    cantidad2 = fields.Integer('Cantidad')
    producto3 = fields.Many2One('product.product', 'Producto Mes 3',
        help='Producto')
    cantidad3 = fields.Integer('Cantidad')

    @classmethod
    def __setup__(cls):
        super(SaludSexual, cls).__setup__()
        cls._order[0] = ('fecha', 'DESC')

    def get_dni(self, usuario):
        return self.usuario.name.ref

    def get_patient_age(self, usuario):
        edad = 'Sin Fecha Nacimiento'
        if self.usuario.name.dob:
            fecha_ent = self.fecha
            nac = self.usuario.name.dob
            delta = relativedelta(fecha_ent, nac)
            edad = ('%sy %sm %sd' %
                (str(delta.years), str(delta.months), str(delta.days)))
        return edad


class LecheMaternaMadres(ModelSQL, ModelView):
    'Programa de entrega de leche a Madres'
    __name__ = 'programas.leche_materna_madres'

    fecha_control = fields.Date('Fecha de Control', required=True)
    usuario = fields.Many2One('gnuhealth.patient', 'Apellido y Nombre',
        required=True)
    dni = fields.Function(fields.Char('DNI'), 'get_dni', searcher='search_dni')
    edad = fields.Function(fields.Char('Edad (a fecha de control)'),
        'get_patient_age')
    gestaciones = fields.Integer('Gestaciones', required=True)
    partos = fields.Integer('Partos', required=True)
    consultas = fields.Integer('Num Consultas', required=True)
    sem_embarazo = fields.Integer('Sem. de embarazo', required=True)
    peso = fields.Float('Peso [kg]', required=True)
    talla = fields.Float('Talla [cm]', required=True)
    imc = fields.Float('IMC')
    diag_nut = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('BP', 'Bajo Peso'),
        ('SP', 'Sobrepeso'),
        ('OB', 'Obesidad'),
        ], 'Diag. Nutricional', sort=False)
    emb = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('P', 'Problematico'),
        ], 'Embarazo', sort=False)
    entrega_1 = fields.Integer('Mes 1  [Kg]')
    entrega_2 = fields.Integer('Mes 2  [Kg]')
    entrega_3 = fields.Integer('Mes 3  [Kg]')
    observaciones = fields.Char('Observaciones')

    @classmethod
    def __setup__(cls):
        super(LecheMaternaMadres, cls).__setup__()
        cls._order[0] = ('fecha_control', 'DESC')

    def get_dni(self, usuario):
        return self.usuario.name.ref

    @classmethod
    def search_dni(cls, name, clause):
        return [('usuario.name.ref',) + tuple(clause[1:])]

    def get_patient_age(self, usuario):
        edad = 'Sin Fecha Nacimiento'
        if self.usuario.name.dob:
            fecha_ent = self.fecha_control
            nac = self.usuario.name.dob
            delta = relativedelta(fecha_ent, nac)
            edad = ('%sy %sm %sd' %
                (str(delta.years), str(delta.months), str(delta.days)))
        return edad

    @fields.depends('peso', 'talla')
    def on_change_with_imc(self):
        if self.talla:
            return float(self.peso) / pow((float(self.talla) / 100), 2)


class LecheMaternaMenores(ModelSQL, ModelView):
    'Programa de entrega de leche a Menores'
    __name__ = 'programas.leche_materna_menores'

    fecha_control = fields.Date('Fecha de Control', required=True)
    usuario = fields.Many2One('gnuhealth.patient', 'Apellido y Nombre',
        required=True)
    dni = fields.Function(fields.Char('DNI'), 'get_dni',
        searcher='search_dni')
    fn = fields.Function(fields.Date('Fecha Nac.'), 'get_fn')
    edad = fields.Function(fields.Char('Edad (a fecha de control)'),
        'get_patient_age')
    peso_nacimiento = fields.Float('Peso al Nacer [Kg]', required=True)
    peso = fields.Float('Peso [Kg]', required=True)
    talla = fields.Float('Talla [cm]', required=True)
    imc = fields.Float('IMC')
    diag_nut = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('RBP', 'Riesgo Bajo Peso'),
        ('BP', 'Bajo Peso'),
        ('SP', 'Sobrepeso'),
        ('OB', 'Obesidad'),
        ('TB', 'Talla Baja'),
        ('CL', 'Crecimiento Lento'),
        ('CA', 'Crecimiento Acelerado'),
        ], 'Diag. Nutricional', sort=False)
    extra = fields.Integer('Extra Progr.')
    entrega_1 = fields.Integer('Mes 1  [Kg]')
    entrega_2 = fields.Integer('Mes 2  [Kg]')
    entrega_3 = fields.Integer('Mes 3  [Kg]')
    observaciones = fields.Char('Observaciones')

    @classmethod
    def __setup__(cls):
        super(LecheMaternaMenores, cls).__setup__()
        cls._order[0] = ('fecha_control', 'DESC')

    def get_dni(self, usuario):
        return self.usuario.name.ref

    @classmethod
    def search_dni(cls, name, clause):
        return [('usuario.name.ref',) + tuple(clause[1:])]

    def get_fn(self, usuario):
        if self.usuario.name.dob:
            return self.usuario.name.dob

    def get_patient_age(self, usuario):
        edad = 'Sin Fecha Nacimiento'
        if self.usuario.name.dob:
            fecha_ent = self.fecha_control
            nac = self.usuario.name.dob
            delta = relativedelta(fecha_ent, nac)
            edad = ('%sy %sm %sd' %
                (str(delta.years), str(delta.months), str(delta.days)))
        return edad

    @fields.depends('peso', 'talla')
    def on_change_with_imc(self):
        if self.talla:
            return float(self.peso) / pow((float(self.talla) / 100), 2)
