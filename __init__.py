# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .z_lister_programas import *


def register():
    Pool.register(
        SaludSexual,
        LecheMaternaMadres,
        LecheMaternaMenores,
        module='z_lister_programas', type_='model')
